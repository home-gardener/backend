{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "home-gardener-backend.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "home-gardener-backend.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "home-gardener-backend.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "home-gardener-backend.labels" -}}
helm.sh/chart: {{ include "home-gardener-backend.chart" . }}
{{ include "home-gardener-backend.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "home-gardener-backend.selectorLabels" -}}
app.kubernetes.io/name: {{ include "home-gardener-backend.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end -}}

{{- define "home-gardener-backend.annotations" -}}
{{- if .Values.CI_PROJECT_PATH_SLUG }}
app.gitlab.com/app: {{ .Values.CI_PROJECT_PATH_SLUG }}
{{- end }}
{{- if .Values.CI_ENVIRONMENT_SLUG }}
app.gitlab.com/env: {{ .Values.CI_ENVIRONMENT_SLUG }}
{{- end }}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "home-gardener-backend.serviceAccountName" -}}
{{- if .Values.serviceAccount.create -}}
    {{ default (include "home-gardener-backend.fullname" .) .Values.serviceAccount.name }}
{{- else -}}
    {{ default "default" .Values.serviceAccount.name }}
{{- end -}}
{{- end -}}
