FROM node:13-alpine as assets

RUN apk update && apk add openssl-dev libssl1.1 wget unzip

USER node
WORKDIR /app

ENV DATABASE_URL="postgres://postgres:example@localhost/postgres"

COPY ./ ./

RUN wget https://gitlab.com/npappas/prisma-engines/-/jobs/574198465/artifacts/download -O prisma-musl.zip && unzip prisma-musl.zip -d prisma-musl-artifacts
RUN mkdir prisma-musl-bin && cp prisma-musl-artifacts/target/`uname -m`-unknown-linux-musl/release/* prisma-musl-bin/

ENV PRISMA_QUERY_ENGINE_BINARY=/app/prisma-musl-bin/query-engine
ENV PRISMA_INTROSPECTION_ENGINE_BINARY=/app/prisma-musl-bin/introspection-engine
ENV PRISMA_MIGRATION_ENGINE_BINARY=/app/prisma-musl-bin/migration-engine

RUN npm ci
RUN npm run build

FROM node:13-alpine

RUN apk update && apk add openssl-dev libssl1.1

USER 1000
WORKDIR /app

COPY --from=assets /app/node_modules /app/node_modules
COPY --from=assets /app/dist /app/dist
COPY --from=assets /app/prisma-musl-bin /app/prisma-musl-bin
COPY ./package*.json /app
COPY ./entrypoint.sh /app/entrypoint.sh
COPY ./prisma /app/prisma

ENV PRISMA_QUERY_ENGINE_BINARY=/app/prisma-musl-bin/query-engine
ENV PRISMA_INTROSPECTION_ENGINE_BINARY=/app/prisma-musl-bin/introspection-engine
ENV PRISMA_MIGRATION_ENGINE_BINARY=/app/prisma-musl-bin/migration-engine

ENTRYPOINT [ "sh", "./entrypoint.sh" ]
