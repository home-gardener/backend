import { User } from '@prisma/client'
import { Algorithm, JwtHeader, SigningKeyCallback, verify } from 'jsonwebtoken'
import * as jwksClient from 'jwks-rsa'
import wretch from 'wretch'
import { dedupe, throttlingCache } from 'wretch-middlewares'
import { Context } from './context'

wretch()
  .middlewares([
    dedupe(),
    throttlingCache({
      throttle: 5 * 60 * 1000, // 5 minutes
    }),
  ])
  .polyfills({
    fetch: require('node-fetch'),
    // FormData: require("form-data"),
    // URLSearchParams: require("url").URLSearchParams
  })

interface Token {
  iss: string
  sub: string
  aud: string[]
  iat: number
  exp: number
  azp: string
  scope: string
  permissions: string[]
}

interface UserInfo {
  sub: string
  given_name: string
  family_name: string
  nickname: string
  name: string
  picture: string
  locale: string
  updated_at: string
  email: string
  email_verified: boolean
}

export interface UserWithPermissions extends User {
  permissions: string[]
}

const WELL_KNOWN = `${process.env.JWT_ISS?.replace(
  /\/$/,
  '',
)}/.well-known/openid-configuration`

const getWellKnown = async (): Promise<{ [key: string]: string }> => {
  const wellKnownData = await wretch(WELL_KNOWN)
    .get()
    .json()
    .catch((err) => {
      throw new Error('Unable to reach well-known')
    })

  return wellKnownData
}

const clientPromise = () =>
  getWellKnown().then((res) =>
    jwksClient({
      rateLimit: true,
      jwksUri: res.jwks_uri,
    }),
  )

const getKey = async (header: JwtHeader, callback: SigningKeyCallback) => {
  if (!header || !header.kid) {
    return callback(new Error('No header'))
  }

  const client = await clientPromise()

  client.getSigningKey(header.kid, (err, key: any) => {
    if (err) {
      return callback(err)
    }
    var signingKey: string = key.publicKey || key.rsaPublicKey
    callback(null, signingKey)
  })
}

export const getUser = async (
  context: Context,
): Promise<UserWithPermissions> => {
  const augmentPermissions = (
    user: User,
    permissions: string[],
  ): UserWithPermissions => {
    return {
      ...user,
      permissions,
    }
  }

  const lookupUserInfoFromRemote = async (authHeader: string) => {
    console.log(`Looking up user from OAuth API`)
    const rawToken: string = authHeader.replace('Bearer ', '')

    const userInfoUrl = (await getWellKnown()).userinfo_endpoint

    const userInfo: UserInfo = await wretch(userInfoUrl!)
      .auth(`Bearer ${rawToken}`)
      .post()
      .json()

    return userInfo
  }

  const token = await getTokenData(context)
  const Authorization = context.request.get('Authorization')
  if (Authorization) {
    const userFromSub = await context.prisma.jWTSub.findOne({
      include: {
        user: {
          select: {
            email: true,
          },
        },
      },
      where: {
        sub: token.sub,
      },
    })

    // skip remote lookup if we have it locally
    const userInfo = !userFromSub
      ? await lookupUserInfoFromRemote(Authorization)
      : null

    const email = userFromSub ? userFromSub.user.email : userInfo?.email

    const user = await context.prisma.user.findOne({
      where: {
        email: email,
      },
    })

    if (user) {
      if (!userFromSub) {
        console.log(
          `User existed but did not have a sub, attaching [${token.sub}]`,
        )
        await context.prisma.user.update({
          where: {
            id: user.id,
          },
          data: {
            jwtSub: {
              create: {
                sub: token.sub,
              },
            },
          },
        })
      }

      return augmentPermissions(user, token.permissions)
    } else if (userInfo) {
      console.log(
        `Creating new user ${userInfo.name} (${userInfo.email}) with sub [${token.sub}]`,
      )
      const newUser = await context.prisma.user.create({
        data: {
          email: userInfo.email,
          name: userInfo.name,
          jwtSub: {
            create: {
              sub: token.sub,
            },
          },
        },
      })

      return augmentPermissions(newUser, token.permissions)
    } else {
      throw new Error('Not Authorized')
    }
  } else {
    throw new Error('Not Authorized')
  }
}

export const getTokenData = (context: Context): Promise<Token> => {
  const Authorization = context.request.get('Authorization')
  if (Authorization) {
    const token: string = Authorization.replace('Bearer ', '')
    return new Promise((resolve, reject) => {
      verify(
        token,
        getKey,
        {
          audience: process.env.JWT_AUD,
          issuer: process.env.JWT_ISS,
          algorithms: process.env.JWT_ALGS?.split(',') as Algorithm[],
        },
        (err, verifiedToken) => {
          if (err) {
            return reject(err)
          }
          if (!verifiedToken) {
            return reject()
          }
          return resolve(verifiedToken as Token)
        },
      )
    })
  } else {
    return Promise.reject(new Error('No Authorization token'))
  }
}
