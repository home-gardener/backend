import * as Sentry from '@sentry/node'
import { GraphQLServer } from 'graphql-yoga'
import { createContext } from './context'
import { permissions } from './permissions'
import { schema } from './schema'

if (process.env.SENTRY_DSN) {
  Sentry.init({
    dsn: process.env.SENTRY_DSN,
  })
}

const corsWhitelist = process.env.CORS_ORIGINS?.split('\n').map((s) =>
  s.startsWith('/') ? new RegExp(s.substring(1, s.length - 1)) : s,
)

new GraphQLServer({
  schema: schema,
  context: createContext,
  middlewares: [permissions],
}).start(
  {
    cors: {
      origin: corsWhitelist,
      methods: ['GET', 'HEAD', 'POST'],
      preflightContinue: false,
      optionsSuccessStatus: 200,
    },
  },
  () => console.log(`🚀 Server ready at: http://0.0.0.0:4000 ⭐️`),
)
