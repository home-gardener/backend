import { enumType } from '@nexus/schema'

export const GardenRole = enumType({
	name: 'GardenRole',
	members: ['ADMIN', 'EDITOR', 'VIEWER'],
})
