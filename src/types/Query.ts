import { queryType } from '@nexus/schema'
import { getUser } from '../utils'

export const Query = queryType({
	definition(t) {
		t.field('me', {
			type: 'User',
			nullable: true,
			resolve: async (parent, args, ctx) => {
				return getUser(ctx)
			},
		})

		t.crud.garden()
		t.crud.plant()
		t.crud.user()
		t.crud.gardenMember()
		t.crud.gardenPixel()

		t.crud.gardens({ ordering: true, filtering: true })
		t.crud.plants({
			ordering: true,
		})
		t.crud.gardenMembers({
			ordering: true,
			filtering: true,
		})
		t.crud.users({
			ordering: true,
		})
		t.crud.gardenPixels({
			ordering: true,

			filtering: true,
		})
	},
})
