import { objectType } from '@nexus/schema'

export const GardenMember = objectType({
	name: 'GardenMember',
	definition(t) {
		t.model.id()
		t.model.user()
		t.model.garden()
		t.model.role()
	},
})
