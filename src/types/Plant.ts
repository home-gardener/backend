import { objectType } from '@nexus/schema'

export const Plant = objectType({
	name: 'Plant',
	definition(t) {
		t.model.id()
		t.model.name()
		t.model.daysToHarvest()
		t.model.gardenPixels()
	},
})
