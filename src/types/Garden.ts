import { objectType } from '@nexus/schema'

export const Garden = objectType({
  name: 'Garden',
  definition(t) {
    t.model.id()
    t.model.memberships()
    t.model.pixels()
    t.model.name()
    t.model.description()
    t.model.notes()
    t.model.topSize()
    t.model.bottomSize()
    t.model.leftSize()
    t.model.rightSize()
    t.model.createdAt()
    t.model.updatedAt()
  },
})
