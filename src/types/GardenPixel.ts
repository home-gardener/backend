import { objectType } from '@nexus/schema'

export const GardenPixel = objectType({
	name: 'GardenPixel',
	definition(t) {
		t.model.id()
		t.model.garden()
		t.model.x()
		t.model.y()
		t.model.notes()
		t.model.color()
		t.model.plantable()
		t.model.plant()
		t.model.plantedBy()
		t.model.harvestedDate()
	},
})
