import { mutationType, stringArg } from '@nexus/schema'
import { GardenRole as GardenRoleEnum } from '@prisma/client'

export const Mutation = mutationType({
  definition(t) {
    t.crud.createOneGarden()
    t.crud.createOneGardenPixel()
    t.crud.createOnePlant()

    t.crud.updateOneGarden()
    t.crud.updateOneGardenPixel()
    t.crud.updateOnePlant()

    t.crud.updateManyGardenPixel()

    t.crud.deleteOneGarden()
    t.crud.deleteOneGardenMember()
    t.crud.deleteOneGardenPixel()
    t.crud.deleteOnePlant()

    t.crud.upsertOneGardenPixel()

    t.field('setGardenMember', {
      type: 'Boolean',
      nullable: false,
      args: {
        id: stringArg({ nullable: true }),
        email: stringArg({ nullable: true }),
        gardenId: stringArg({ nullable: false }),
        role: stringArg({ nullable: false }),
      },
      resolve: async (
        parent,
        {
          id,
          email,
          gardenId,
          role,
        }: {
          id?: string | null
          email?: string | null
          gardenId: string
          role: string
        },
        ctx,
      ) => {
        let gardenMember
        if (id) {
          gardenMember = await ctx.prisma.gardenMember.update({
            where: {
              id: id,
            },
            data: {
              role: role as GardenRoleEnum,
            },
          })
        } else if (email) {
          const foundUser = await ctx.prisma.user.findOne({
            where: {
              email: email,
            },
          })

          if (foundUser) {
            // update if exists
            const existingGardenRole = await ctx.prisma.gardenMember.findOne({
              where: {
                userId_gardenId: {
                  gardenId: gardenId,
                  userId: foundUser.id,
                },
              },
            })

            if (existingGardenRole) {
              gardenMember = await ctx.prisma.gardenMember.update({
                where: {
                  id: existingGardenRole.id,
                },
                data: {
                  role: role as GardenRoleEnum,
                },
              })
            } else {
              gardenMember = await ctx.prisma.gardenMember.create({
                data: {
                  garden: {
                    connect: {
                      id: gardenId,
                    },
                  },
                  user: {
                    connect: {
                      id: foundUser.id,
                    },
                  },
                  role: role as GardenRoleEnum,
                },
              })
            }
          } else {
            gardenMember = await ctx.prisma.gardenMember.create({
              data: {
                garden: {
                  connect: {
                    id: gardenId,
                  },
                },
                user: {
                  create: {
                    email: email,
                    name: email,
                  },
                },
                role: role as GardenRoleEnum,
              },
            })
          }
        }

        return !!gardenMember
      },
    })
  },
})
