import {
  Enumerable,
  FindManyGardenArgs,
  FindManyGardenMemberArgs,
  FindManyGardenPixelArgs,
  FindOneUserArgs,
  GardenCreateArgs,
  GardenDeleteArgs,
  GardenMemberDeleteArgs,
  GardenPixelCreateArgs,
  GardenPixelDeleteArgs,
  GardenPixelUpdateArgs,
  GardenPixelUpsertArgs,
  GardenRole as GardenRoleEnum,
  GardenUpdateArgs,
  PlantCreateArgs,
  User,
} from '@prisma/client'
import { and, or, rule, shield } from 'graphql-shield'
import { Context } from '../context'
import { getUser, UserWithPermissions } from '../utils'

const isSingleType = <T>(obj: Enumerable<T>): obj is T => {
  if ((obj as any)?.length === undefined) {
    return true
  } else {
    return false
  }
}

const isAdminUser = (user: UserWithPermissions) => {
  return (
    user.permissions.includes('delete:plant') &&
    user.permissions.includes('update:plant') &&
    user.permissions.includes('create:garden') &&
    user.permissions.includes('read:gardens') &&
    user.permissions.includes('delete:gardens')
  )
}

const getGardenMembership = async (
  user: User,
  gardenId: string | null | undefined,
  context: Context,
) => {
  if (!gardenId) {
    return null
  }
  const membership = await context.prisma.gardenMember.findOne({
    where: {
      userId_gardenId: {
        gardenId: gardenId,
        userId: user.id,
      },
    },
  })

  return membership
}

const rules = {
  isAuthenticatedUser: rule()(async (parent, args, context: Context) => {
    const user = await getUser(context)
    return Boolean(user)
  }),

  isAdministratorUser: rule()(async (parent, args, context: Context) => {
    const user = await getUser(context)
    return isAdminUser(user)
  }),

  canCreateGarden: rule()(async (parent, args, context: Context) => {
    const user = await getUser(context)
    return isAdminUser(user) || user.permissions.includes('create:garden')
  }),

  canCreatePlant: rule()(
    async (parent, { data }: PlantCreateArgs, context: Context) => {
      const user = await getUser(context)
      return user.permissions.includes('create:plant')
    },
  ),

  canUpdatePlant: rule()(
    async (parent, { data }: PlantCreateArgs, context: Context) => {
      const user = await getUser(context)
      return user.permissions.includes('update:plant')
    },
  ),

  canDeletePlant: rule()(async (parent, args, context: Context) => {
    const user = await getUser(context)
    return isAdminUser(user) || user.permissions.includes('delete:plant')
  }),

  // this still kind of needs some work - doesn't secure all cases
  isSelf: rule()(async (parent, args: FindOneUserArgs, context: Context) => {
    if (!args.where) {
      return true
    }

    const user = await getUser(context)

    let field: 'id' | 'email' = 'id'
    if (args.where.email) {
      field = 'email'
    }

    const targetUser = await context.prisma.user.findOne({
      where: {
        [field]: args.where[field],
      },
    })

    return !!user && user.id === targetUser?.id
  }),

  isCreatingGardenAsSelf: rule()(
    async (parent, { data }: GardenCreateArgs, context: Context) => {
      if (!data.memberships?.create) {
        return false
      }

      const user = await getUser(context)

      if (isSingleType(data.memberships.create)) {
        if (data.memberships.create.role !== 'ADMIN') {
          return false
        }

        const userUser = await context.prisma.user.findOne({
          where: {
            id: data.memberships.create.user.connect?.id,
          },
        })

        return user?.id === userUser?.id
      } else if (data.memberships.create.length === 1) {
        if (data.memberships.create[0].role !== 'ADMIN') {
          return false
        }

        const userUser = await context.prisma.user.findOne({
          where: {
            id: data.memberships.create[0].user.connect?.id,
          },
        })

        return user?.id === userUser?.id
      } else {
        return false
      }
    },
  ),

  canUpdateGarden: rule()(
    async (parent, { data, where }: GardenUpdateArgs, context: Context) => {
      const user = await getUser(context)
      const membership = await getGardenMembership(user, where.id, context)

      if (membership?.role === 'ADMIN' || membership?.role === 'EDITOR') {
        return true
      }

      return false
    },
  ),

  canDeleteGarden: rule()(
    async (parent, { where }: GardenDeleteArgs, context: Context) => {
      const user = await getUser(context)
      const membership = await getGardenMembership(user, where.id, context)

      if (membership?.role === 'ADMIN') {
        return true
      }

      return false
    },
  ),

  canSetGardenMember: rule()(
    async (
      parent,
      {
        data,
      }: {
        data: {
          email?: string
          id?: string
          gardenId: string
          role: GardenRoleEnum
        }
      },
      context: Context,
    ) => {
      const user = await getUser(context)
      const myMembership = await getGardenMembership(
        user,
        data.gardenId,
        context,
      )
      let affectedUser
      affectedUser = await context.prisma.user.findOne({
        where: { id: data.id },
      })
      if (!affectedUser) {
        affectedUser = await context.prisma.user.findOne({
          where: { email: data.email },
        })
      }

      if (myMembership?.role === 'ADMIN') {
        if (affectedUser?.id === user.id) {
          // Admin shouldn't be able to modify self at this point
          return false
        } else {
          return true
        }
      }

      return false
    },
  ),

  canDeleteGardenMember: rule()(
    async (parent, { where }: GardenMemberDeleteArgs, context: Context) => {
      const user = await getUser(context)
      const gardenMember = await context.prisma.gardenMember.findOne({
        where,
      })

      const myMembership = await getGardenMembership(
        user,
        gardenMember?.gardenId,
        context,
      )

      if (myMembership?.role === 'ADMIN' && gardenMember?.userId !== user.id) {
        return true
      }

      return false
    },
  ),

  canCreateGardenPixel: rule()(
    async (parent, { data }: GardenPixelCreateArgs, context: Context) => {
      const user = await getUser(context)
      const garden = await context.prisma.garden.findOne({
        where: {
          id: data.garden.connect?.id,
        },
      })

      const myMembership = await getGardenMembership(user, garden?.id, context)

      if (myMembership?.role === 'ADMIN' || myMembership?.role === 'EDITOR') {
        return true
      }

      return false
    },
  ),

  canUpdateGardenPixel: rule()(
    async (
      parent,
      { data, where }: GardenPixelUpdateArgs,
      context: Context,
    ) => {
      const user = await getUser(context)
      const gardenPixel = await context.prisma.gardenPixel.findOne({
        where: {
          id: where.id,
        },
      })

      const myMembership = await getGardenMembership(
        user,
        gardenPixel?.gardenId,
        context,
      )

      if (myMembership?.role === 'ADMIN' || myMembership?.role === 'EDITOR') {
        return true
      }

      return false
    },
  ),

  canUpsertGardenPixel: rule()(
    async (
      parent,
      { create, update, where }: GardenPixelUpsertArgs,
      context: Context,
    ) => {
      const user = await getUser(context)
      const gardenPixel = await context.prisma.gardenPixel.findOne({
        where: {
          id: where.id,
        },
      })

      if (gardenPixel) {
        // updating
        const myMembership = await getGardenMembership(
          user,
          gardenPixel?.gardenId,
          context,
        )

        if (myMembership?.role === 'ADMIN' || myMembership?.role === 'EDITOR') {
          return true
        }

        return false
      } else {
        // creating
        const myMembership = await getGardenMembership(
          user,
          create.garden.connect?.id,
          context,
        )
        if (myMembership?.role === 'ADMIN' || myMembership?.role === 'EDITOR') {
          return true
        }

        return false
      }
    },
  ),

  canDeleteGardenPixel: rule()(
    async (parent, { where }: GardenPixelDeleteArgs, context: Context) => {
      const user = await getUser(context)
      const gardenPixel = await context.prisma.gardenPixel.findOne({
        where,
      })

      const myMembership = await getGardenMembership(
        user,
        gardenPixel?.gardenId,
        context,
      )

      if (myMembership?.role === 'ADMIN' || myMembership?.role === 'EDITOR') {
        return true
      }

      return false
    },
  ),

  canGetGardenMembers: rule()(
    async (parent, { where }: FindManyGardenMemberArgs, context: Context) => {
      const user = await getUser(context)
      const gardenMembers = await context.prisma.gardenMember.findMany({
        where,
        include: {
          garden: {
            include: {
              memberships: {
                where: {
                  userId: user.id,
                },
              },
            },
          },
        },
      })

      return gardenMembers.every((gardenMember) => {
        gardenMember.garden.memberships.length > 0
      })
    },
  ),

  canGetGardenPixels: rule()(
    async (parent, { where }: FindManyGardenPixelArgs, context: Context) => {
      const user = await getUser(context)
      const gardenPixels = await context.prisma.gardenPixel.findMany({
        where,
        include: {
          garden: {
            include: {
              memberships: {
                where: {
                  userId: user.id,
                },
              },
            },
          },
        },
      })

      return gardenPixels.every((gardenPixel) => {
        gardenPixel.garden.memberships.length > 0
      })
    },
  ),

  canGetGardens: rule()(
    async (parent, { where }: FindManyGardenArgs, context: Context) => {
      const user = await getUser(context)
      if (user.permissions.includes('read:gardens')) {
        return true
      }

      const gardens = await context.prisma.garden.findMany({
        where,
        include: {
          memberships: {
            where: {
              userId: user.id,
            },
          },
        },
      })

      return gardens.every((garden) => garden.memberships.length !== 0)
    },
  ),
}

export const permissions = shield(
  {
    Query: {
      me: rules.isAuthenticatedUser,
      gardenMembers: rules.canGetGardenMembers,
      gardenPixels: rules.canGetGardenPixels,
      gardens: rules.canGetGardens,
    },
    User: {
      email: or(rules.isAdministratorUser, rules.isSelf),
    },
    Mutation: {
      // XXX secure the create routes so that you can
      // only create and link under the right circumstances
      createOneGarden: and(rules.canCreateGarden, rules.isCreatingGardenAsSelf),
      createOneGardenPixel: rules.canCreateGardenPixel,
      createOnePlant: rules.canCreatePlant,

      updateOneGarden: rules.canUpdateGarden,
      updateOneGardenPixel: rules.canUpdateGardenPixel,
      updateManyGardenPixel: rules.canUpdateGardenPixel,
      upsertOneGardenPixel: rules.canUpsertGardenPixel,
      updateOnePlant: rules.canUpdatePlant,

      setGardenMember: rules.canSetGardenMember,

      deleteOneGarden: rules.canDeleteGarden,
      deleteOneGardenMember: rules.canDeleteGardenMember,
      deleteOneGardenPixel: rules.canDeleteGardenPixel,
      deleteOnePlant: rules.canDeletePlant,
    },
  },
  {
    allowExternalErrors: true,
  },
)
