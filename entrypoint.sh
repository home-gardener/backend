#!/bin/sh

echo 'Running migrations...'
npx prisma migrate up --auto-approve --experimental

echo 'Checking if seeding is needed...'
npm run seed

echo 'Starting server...'
npm run start
