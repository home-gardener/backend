#!/bin/bash
BUILD_ID=`git rev-parse --short HEAD`

docker buildx build --platform linux/arm64/v8,linux/amd64 --pull -f Dockerfile -t radicand/home-gardener-backend:latest -t radicand/home-gardener-backend:$BUILD_ID .
# docker buildx build --platform linux/arm64/v8,linux/amd64 --pull -f Dockerfile -t radicand/home-gardener-backend:latest -t radicand/home-gardener-backend:$BUILD_ID --push .
