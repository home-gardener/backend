import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

async function main() {
	if ((await prisma.user.count()) > 0) {
		console.log(`Not seeding the database - users table already has data.`)
		return
	}

	const email = process.env.EMAIL
	const name = process.env.NAME

	if (!email || !name) {
		console.log(
			`Please set the EMAIL and NAME environmental variables to properly bootstrap the database.`,
		)
		return
	}

	const user1 = await prisma.user.upsert({
		where: {
			email,
		},
		create: {
			email,
			name,
		},
		update: {
			name,
		},
	})

	console.log({ user1 })
}

main().finally(async () => {
	await prisma.disconnect()
})
